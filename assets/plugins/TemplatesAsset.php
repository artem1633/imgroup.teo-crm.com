<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class TemplatesAsset
 * @package app\assets\plugins
 */
class TemplatesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'libs/ckeditor/ckeditor.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
    ];
}