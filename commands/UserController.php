<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\helpers\Console;

class UserController extends Controller
{
    public function actionRegenerateApiKeys()
    {
        $users = User::find()->all();

        foreach ($users as $user)
        {
            $user->updateApiKey();
        }

        $this->stdout("API ключи обновлены\n", Console::FG_YELLOW);
    }
}

?>