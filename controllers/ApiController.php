<?php

namespace app\controllers;

use Yii;
use app\models\Documents;
use app\models\TemplateFields;
use app\models\Templates;
use app\models\User;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ApiController
 */
class ApiController extends Controller
{

    const API_KEY = 'WUUXLbHScg813mShY5BMfL0KDsv9B0RK';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Main API Method
     * @return mixed
     */
    public function actionIndex()
    {
        $this->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $post = $request->post();

        if(!isset($post['api_key']))
            return ['error' => 'Ошибка авторизациии'];

        if($post['api_key'] != self::API_KEY)
            return ['error' => 'Ошибка авторизациии. API Ключ не верный!'];

        if(!isset($post['template_id']))
            return ['error' => 'нет обязательных параметров'];

        $template = Templates::findOne($post['template_id']);

        if($template == null)
            return ['error' => 'Шаблон под id '.$post['template_id'].' не найден'];


        /** @var \app\models\TemplateFields[] $fields */
        $fields = $template->getTemplateFields()->where(['type' => TemplateFields::TYPE_POST])->all();
        $fieldsNames = [];

        $cacheKeys = [];

        foreach ($fields as $field)
        {
            if(isset($post['Post'][$field->name]))
            {
                Yii::$app->cache->set("templates.{$field->template_id}.{$field->name}", $post['Post'][$field->name]);
                $cacheKeys["templates.{$field->template_id}.{$field->name}"] = $post['Post'][$field->name];
            }
        }

        return $cacheKeys;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
}
