<?php

namespace app\controllers;

use app\helpers\DirHelper;
use app\helpers\StringHelper;
use app\models\DocumentSection;
use app\models\TemplateFields;
use app\models\Templates;
use app\models\User;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use app\models\Documents;
use app\models\DocumentsSearch;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Changing;
use app\models\DocumentsField;

/**
 * DocumentsController implements the CRUD actions for Documents model.
 */
class DocumentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'print', 'view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionLoginTest()
    {
        $url = Url::toRoute(['documents/index'], true);
        echo "	<form action=\"{$url}\" method=\"post\" enctype=\"multipart/form-data\">
		<input type=\"text\" placeholder='api_key' name=\"api_key\">
		<input type=\"text\" placeholder='template_id' name=\"template_id\">
		<input type=\"text\" placeholder='Post[post_field]' name=\"Post[post_field]\">
		<input type=\"text\" placeholder='Post[post]' name=\"Post[post]\">
		<input type=\"submit\">
	</form>";
    }

    /**
     * Lists all Documents models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $post = $request->post();

            if(!isset($post['api_key']))
                return ['error' => 'Ошибка авторизациии'];

            /** @var \app\models\User $user */
            $user = User::find()->where(['api_key' => $post['api_key']])->one();

            if($user != null)
            {
                Yii::$app->user->login($user, 3600*24*30);
            } else {
                return ['error' => 'Ошибка авторизациии. API Ключ не верный!'];
            }

            if(!isset($post['template_id']))
                return ['error' => 'нет обязательных параметров'];

            $template = Templates::findOne($post['template_id']);

            if($template == null)
                return ['error' => 'Шаблон под id '.$post['template_id'].' не найден'];


            /** @var \app\models\TemplateFields[] $fields */
            $fields = $template->getTemplateFields()->where(['type' => TemplateFields::TYPE_POST])->all();

            foreach ($fields as $field)
            {
                if(isset($post['Post'][$field->name]))
                {
                    Yii::$app->cache->set("templates.{$field->template_id}.{$field->name}", $post['Post'][$field->name]);
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_HTML;

        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChanging($table_name)
    {    
        $query = Changing::find()->where(['table_name' => $table_name ]);
        $dataProvider = new ActiveDataProvider([ 'query' => $query, ]);

        return $this->render('changing', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionPreview($id)
    {
        $model = $this->findModel($id);

        $path = $model->getPreviewHref();

        $html = file_get_contents($path);

        $html = str_replace("=\"/tilda/", '="/'.$model->getSiteFolderPath()."/tilda/", $html);
        $html = str_replace("('/tilda/", '(\'/'.$model->getSiteFolderPath()."/tilda/", $html);

        $file = fopen($model->getSiteFolderPath().'/index_preview.php', 'w');

        fwrite($file, $html);

        fclose($file);

        return $this->renderPartial('preview', [
            'href' => $model->getSiteFolderPath().'/index_preview.php',
        ]);
    }

    /**
     * Displays a single Documents model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $query = Changing::find()->where(['table_name' => 'document', 'line_id' => $id ]);
        $dataProvider = new ActiveDataProvider([ 'query' => $query, ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Documents model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($templateId = null)
    {
        $request = Yii::$app->request;
        $model = new Documents();

        if($templateId){
            $model->template_id = $templateId;
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить документ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'fields' => [],
                        'fieldsModel' => null,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Далее',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){
                $template = $model->template;
                $fields = $template->templateFields;
                $fieldsNames = [];

                if($model->isNewRecord == false){
                    $sections = DocumentSection::find()->where(['document_id' => $model->id])->all();
                } else {
                    $sections = [];
                }

                foreach ($fields as $field)
                {
                    $fieldsNames[] = $field->name;
                    if($field->type === TemplateFields::TYPE_HTML)
                        $fieldsNames[] = $field->name.'_show';
                }

                $fieldsModel = new DynamicModel($fieldsNames);

                if(isset($request->post()['DynamicModel']))
                    $model->scenario = Documents::SCENARIO_FIELDS;
                if(isset($request->post()['sections_sort']))
                    $model->scenario = Documents::SCENARIO_SECTIONS;

                \Yii::warning($model->scenario, 'Model scenario');

                if($model->scenario === Documents::SCENARIO_DEFAULT)
                {
                    $model->scenario = Documents::SCENARIO_FIELDS;

                    return [
                        'title'=> "Добавить поля к документу «{$model->name}»",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                            'fields' => $fields,
                            'fieldsModel' => $fieldsModel,
                            'sections' => $sections,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Далее',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                } else if($model->scenario === Documents::SCENARIO_FIELDS)
                {
                    if($fieldsModel->load($request->post()))
                    {
                        $htmlFields = array_values(ArrayHelper::map(array_filter($fields, function($field) { return $field->type === TemplateFields::TYPE_HTML; }), 'id', 'name'));
                        $fields = $request->post()['DynamicModel'];


                        $postFields = $template->getTemplateFields()->where(['type' => TemplateFields::TYPE_POST])->all();
                        foreach ($postFields as $field)
                        {
                            if(($value = Yii::$app->cache->get("templates.{$template->id}.{$field->name}")) != null)
                            {
                                $fields[$field->name] = $value;
                            }
                        }

                        $str = $template->content;
                        $out = null;

                        $model->content = preg_replace_callback('/{[a-zA-Z0-9._-]+}/', function($matches) use ($fields, $htmlFields) {
                            foreach ($matches as $match) {
                                $match = trim($match, '\{\}');

                                if(in_array($match, $htmlFields) && ArrayHelper::getValue($fields, $match.'_show') == null)
                                {
                                    return null;
                                }

                                return ArrayHelper::getValue($fields, $match);
                            }
                        },$str);

                        $model->save(false);
                        foreach ($fields as $key => $value) {
                            $id = TemplateFields::find()->where(['name' => $key, 'template_id' => $model->template_id])->one()->id;
                            $doc_field = DocumentsField::find()->where(['document_id' => $model->id, 'field_id' => $id])->one();


                            if($doc_field == null)
                            {
                                $doc_field = new DocumentsField();
                                $doc_field->document_id = $model->id;
                                $doc_field->field_id = $id;
                                $doc_field->value = $value;
                                $doc_field->save();
                            }
                            else 
                            {
                                $doc_field->value = $value;
                                $doc_field->save();
                            }
                        }

                        $model->save(false, null, true);


                        $model->scenario = Documents::SCENARIO_SECTIONS;

                        $sections = DocumentSection::find()->where(['document_id' => $model->id])->orderBy('sort asc')->all();

                        return [
                            'title'=> "Настроить блоки «{$model->name}»",
                            'content'=>$this->renderAjax('create', [
                                'model' => $model,
                                'fields' => $fields,
                                'fieldsModel' => $fieldsModel,
                                'sections' => $sections,
                            ]),
                            'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                        ];
                    }
                } else if($model->scenario === Documents::SCENARIO_SECTIONS) {
                    $model->id = $request->post()['Documents']['id'];
                    $sort = explode(',', $request->post()['sections_sort']);

                    $counter = 0;
                    foreach ($sort as $s){
                        DocumentSection::updateAll(['sort' => $counter], ['id' => $s]);
                        $counter++;
                    }

                    $html = file_get_contents($model->getPreviewHref());

                    $dom = HtmlDomParser::str_get_html($html);

                    $sections = DocumentSection::find()->where(['document_id' => $model->id])->orderBy('sort asc')->all();

                    $allRecords = $dom->find('#allrecords');
                    $allRecords[0]->innertext = '';

                    $allRecordsInnerHtml = '';

                    foreach ($sections as $section){
                        $allRecordsInnerHtml .= $section->content;
                    }

                    $allRecords[0]->innertext = $allRecordsInnerHtml;

                    $output = $dom;

                    $file = fopen($model->getPreviewHref(), 'w');

                    fwrite($file, $output);

                    fclose($file);

                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'forceClose'=>true,
                    ];
                }
            }else{           
                return [
                    'title'=> "Добавить документ",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'fields' => [],
                        'fieldsModel' => null,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'fields' => [],
                    'fieldsModel' => null,
                ]);
            }
        }
    }

    /**
     * Отдает документ в формате PDF
     * @param $id
     */
    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        $mpdf = new Mpdf();
        $mpdf->SetTitle($model->name);
        $mpdf->WriteHTML($model->content);
        echo $mpdf->Output($model->name.'.pdf', Destination::INLINE);
    }

    /**
     * Updates an existing Documents model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $old_model = $this->findModel($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->validate())
        {
            $template = $model->template;
            $fields = /*TemplateFields::find()->where(['template_id' => $model->template_id])->all();//*/$template->templateFields;
            $fieldsNames = [];

            foreach ($fields as $field)
            {
                $fieldsNames[] = $field->name;
                if($field->type === TemplateFields::TYPE_HTML)
                    $fieldsNames[] = $field->name.'_show';
            }

            $fieldsModel = new DynamicModel($fieldsNames);

            if(isset($request->post()['DynamicModel']))
                $model->scenario = Documents::SCENARIO_FIELDS;
            if(isset($request->post()['sections_sort']))
                $model->scenario = Documents::SCENARIO_SECTIONS;

            if($model->scenario === Documents::SCENARIO_DEFAULT)
            {
                $model->scenario = Documents::SCENARIO_FIELDS;

                /*return $this->render('create', [
                    'model' => $model,
                        'fields' => $fields,
                        'fieldsModel' => $fieldsModel,
                ]);*/
                return [
                    'title'=> "Добавить поля к документу «{$model->name}»",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'fields' => $fields,
                        'fieldsModel' => $fieldsModel,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Далее',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            } 
            else if($model->scenario === Documents::SCENARIO_FIELDS)
            {
                if($fieldsModel->load($request->post()))
                {
                    $htmlFields = array_values(ArrayHelper::map(array_filter($fields, function($field) { return $field->type === TemplateFields::TYPE_HTML; }), 'id', 'name'));
                    $fields = $request->post()['DynamicModel'];

                    foreach ($fields as $key => $value) {
                        //echo "key=".$key.', value = '.$value . '<br>';
                        $id = TemplateFields::find()->where(['name' => $key, 'template_id' => $model->template_id])->one()->id;
                        $doc_field = DocumentsField::find()->where(['document_id' => $model->id, 'field_id' => $id])->one();

                        if($doc_field == null)
                        {
                            $doc_field = new DocumentsField();
                            $doc_field->document_id = $model->id;
                            $doc_field->field_id = $id;
                            $doc_field->value = $value;
                            $doc_field->save();
                        }
                        else 
                        {
                            //echo "<br>old=".$doc_field->value.'; new = '.$value."<br>";
//                            if($value != $doc_field->value) $model->setToChangeTable($key, $doc_field->value, $value);
                            $doc_field->value = $value;
                            $doc_field->save();
                        }
                    }
                    //die;

                    $postFields = $template->getTemplateFields()->where(['type' => TemplateFields::TYPE_POST])->all();
                    foreach ($postFields as $field)
                    {
                        if(($value = Yii::$app->cache->get("templates.{$template->id}.{$field->name}")) != null)
                        {
                            $fields[$field->name] = $value;
                        }
                    }

                    $str = $template->content;
                    $out = null;

                    $model->content = preg_replace_callback('/{[a-z._-]+}/', function($matches) use ($fields, $htmlFields) 
                    {
                        foreach ($matches as $match) 
                        {
                            $match = trim($match, '\{\}');

                            if(in_array($match, $htmlFields) && ArrayHelper::getValue($fields, $match.'_show') == null)
                            {
                                return null;
                            }

                            return ArrayHelper::getValue($fields, $match);
                        }
                    },$str);

                    $model->save(false, null, true);

                    $model->scenario = Documents::SCENARIO_SECTIONS;

                    $sections = DocumentSection::find()->where(['document_id' => $model->id])->orderBy('sort asc')->all();

                    return [
                        'title'=> "Настроить блоки «{$model->name}»",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                            'fields' => $fields,
                            'fieldsModel' => $fieldsModel,
                            'sections' => $sections,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                    /*return $this->redirect(['view', 'id' => $model->id]);*/
                }
            } else if($model->scenario === Documents::SCENARIO_SECTIONS) {
                $model->id = $request->post()['Documents']['id'];
                $sort = explode(',', $request->post()['sections_sort']);

                $counter = 0;
                foreach ($sort as $s){
                    DocumentSection::updateAll(['sort' => $counter], ['id' => $s]);
                    $counter++;
                }

                $html = file_get_contents($model->getPreviewHref());

                $dom = HtmlDomParser::str_get_html($html);

                $sections = DocumentSection::find()->where(['document_id' => $model->id])->orderBy('sort asc')->all();

                $allRecords = $dom->find('#allrecords');
                $allRecords[0]->innertext = '';

                $allRecordsInnerHtml = '';

                foreach ($sections as $section){
                    $allRecordsInnerHtml .= $section->content;
                }

                $allRecords[0]->innertext = $allRecordsInnerHtml;

                $output = $dom;

                $file = fopen($model->getPreviewHref(), 'w');

                fwrite($file, $output);

                fclose($file);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }
        }
        else
        {
            return [
                'title'=> "Редактировать сайт",
                'content'=>$this->renderAjax('create', [
                    'model' => $model,
                    'fields' => [],
                    'fieldsModel' => null,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Далее',['class'=>'btn btn-primary','type'=>"submit"])
        
            ];         
            /*return $this->render('create', [
                    'model' => $model,
                    'fields' => [],
                    'fieldsModel' => null,
                ]);*/
        }
    }

    /**
     * @param int $id
     */
    public function actionDownload($id)
    {
        $model = $this->findModel($id);

        $template = $model->template;
        $contentDir = dirname($template->content);

        if(file_exists('tmp.zip')){
            unlink('tmp.zip');
        }

        if(is_dir('site')){
            DirHelper::deleteDir('site');
        }

        if($template)
        {
            $siteFolderPath = $model->getSiteFolderPath();

            if(is_dir($siteFolderPath)){

                DirHelper::recursiveCopy($siteFolderPath, 'site');

                // Формирование ZIP-Архива
                $zip = new \ZipArchive();
                $zip->open("tmp.zip", \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

//                    $zip->addFile("{$contentDir}/{$targetDir}");

// Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator("site"),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {

                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = explode('web', substr($filePath, strlen("site") + 1))[1];



                        $zip->addFile($filePath, $relativePath);
                    }
                }

                $zip->close();

                Yii::$app->response->sendFile('tmp.zip', $model->name.'.zip');
            }
        }
    }

    /**
     * Delete an existing Documents model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Documents model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteChanging($id)
    {
        $request = Yii::$app->request;
        Changing::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }
    public function actionBulkDeleteChanging()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = Changing::findOne($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if($action->id == 'index')
            $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
