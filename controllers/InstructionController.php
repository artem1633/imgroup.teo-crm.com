<?php

namespace app\controllers;

use Yii;
use app\models\Instruction;
use app\models\User;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * InstructionController
 */
class InstructionController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Render instruction
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Instruction::getInstruction();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Edit instruction
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionEdit()
    {
//        if(Yii::$app->user->identity->role != User::ROLE_ADMIN)
//            throw new ForbiddenHttpException('Доступ запрещен');

        $model = Instruction::getInstruction();

        if($model->load(Yii::$app->request->post()))
        {
            $result = $model->save();
            if($result == true)
                Yii::$app->session->setFlash('success', 'Инструкция успешно изменена');
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }
}
