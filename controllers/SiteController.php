<?php

namespace app\controllers;

use app\helpers\RarHelper;
use app\helpers\StringHelper;
use app\models\DocumentSection;
use app\models\forms\ResetPasswordForm;
use app\models\TemplateFields;
use Codeception\Util\Template;
use PhpQuery\PhpQuery;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

use app\models\Logs;
use app\models\RegisterForm;
use app\models\ResetPasswordForms;
use ZipArchive;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {
//        $html = file_get_contents("parse.html");
//
//        $dom = HtmlDomParser::str_get_html($html);
//
//        $elements = $dom->find('#allrecords .t-rec');
//
//        var_dump(count($elements));


//        var_dump(StringHelper::validateImageUrl("https://assets.newatlas.com/dims4/default/1aca086/2147483647/strip/true/crop/1992x1328+4+0/resize/1200x800!/quality/90/?url=http%3A%2F%2Fnewatlas-brightspot.s3.amazonaws.com%2Ffe%2F06%2F75f2c8704c71ade9c8881c997c8f%2Fdepositphotos-41105113-l-2015.jpg"));
//        var_dump(StringHelper::validateImageUrl("https://www.test.com"));
//        var_dump(StringHelper::validateImageUrl("ltgkjsekjt536t33k"));
//        var_dump(StringHelper::validateImageUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvnsHsfkxlZ1yYJsF5CNOlWD42J6FI7ofv0A&usqp=CAU"));

//        $ch = curl_init('https://content.presspage.com/uploads/2379/1920_ocean-sunset-195865.jpg?10000');
//
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        $result = curl_exec($ch);
//        $info = curl_getinfo($ch);
//
//        curl_close($ch);
//
//        VarDumper::dump($info, 10, true);
//
//        var_dump(StringHelper::validateImageUrl('https://assets.newatlas.com/dims4/default/1aca086/2147483647/strip/true/crop/1992x1328+4+0/resize/1200x800!/quality/90/?url=http%3A%2F%2Fnewatlas-brightspot.s3.amazonaws.com%2Ffe%2F06%2F75f2c8704c71ade9c8881c997c8f%2Fdepositphotos-41105113-l-2015.jpg'));
//        var_dump(StringHelper::validateImageUrl('https://static.bangkokpost.com/media/content/20200114/c1_3487569.jpg'));
//        var_dump(StringHelper::validateImageUrl('https://w7.pngwing.com/pngs/236/279/png-transparent-blue-body-of-water-water-resources-sea-sky-pattern-sea-waves-blue-rectangle-ocean.png'));
//        var_dump(StringHelper::validateImageUrl('https://download.seaicons.com/download/i78441/iynque/flurry-extras-9/iynque-flurry-extras-9-ocean-waves.ico'));
//        var_dump(StringHelper::validateImageUrl('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQC0x84X9MvISEEGi1YTRuJf5sqYrQae3JxEA&usqp=CAU'));
//        var_dump(StringHelper::validateImageUrl('35o0ijtmg09u3h4vgb'));
    }

    private function request($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        VarDumper::dump($info, 10, true);
    }

    /**
     * Регистрирует нового пользователя
     * @return string|Response
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $this->layout = 'main-login';

        $model = new RegisterForm();

        if($model->load(Yii::$app->request->post()) && $model->register())
        {
            Yii::$app->session->setFlash('register_success', 'Регистрация прошла успешно. Пожалуйста, авторизируйтесь');
            return $this->redirect(['login']);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new ResetPasswordForms();

        if($model->load(Yii::$app->request->post()) && $model->reset())
        {
            Yii::$app->session->setFlash('register_success', 'На вашу почту был выслан временный пароль. Воспользуйтесь им для авторизации');
            return $this->redirect(['login']);
        } else {
            return $this->render('reset', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
