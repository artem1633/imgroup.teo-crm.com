<?php

namespace app\controllers;

use app\helpers\StringHelper;
use Yii;
use app\models\TemplateFields;
use app\models\TemplateFieldsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;


/**
 * TemplateFieldsController implements the CRUD actions for TemplateFields model.
 */
class TemplateFieldsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TemplateFields models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TemplateFieldsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single TemplateFields model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "поле #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new TemplateFields model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($word = null, $templateId = null)
    {
        $request = Yii::$app->request;
        $model = new TemplateFields();  

        $hideTemplate = false;

        if($word != null){
            $word = trim($word);

            $repeat = TemplateFields::find()->where(['template_id' => $templateId, 'label' => $word])->count();

            if($repeat > 0){
                $repeat = TemplateFields::find()->where(['template_id' => $templateId])->count();
                $word = $word.'_'.$repeat;
            }

            $model->label = trim($word);
            $model->name = StringHelper::rus2translit($word);
            if($templateId != null){
                $model->template_id = $templateId;
                $hideTemplate = true;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить поле",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'hideTemplate' => $hideTemplate,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){

                if($word != null){

                    $tags = TemplateFields::find()->where(['template_id' => $templateId])->all();

                    $commandScript = '';
                    $menuScript = '';
                    $itemsScript = '';

                    $counter = 1;
                    foreach ($tags as $tag)
                    {
                        $order = 25 + $counter;

                        $commandScript .= "
                            editor.addCommand('insertTag{$tag->id}', {
                              exec: function(editor){  
                                  editor.insertText('{{$tag->name}}');
                              }
                            });
                        ";

                        $menuScript .= "
                            editor.addMenuItems({
                                  tag{$tag->id} :
                                  {
                                    label : '{$tag->label}',
                                    group : 'tags',
                                    command : 'insertTag{$tag->id}',
                                    order : {$order}
                                  },
                              });
                        ";

                        $itemsScript .= "
                            tag{$tag->id}: CKEDITOR.TRISTATE_OFF,
                        ";

                        $counter++;
                    }

                    return [ 'forceClose' => true, 'forceReload' => '#tags-pjax-container', 'afterClose' => '
                        window.editor.insertText("{'.$model->name.'}");
                        
                         // window.editor.addMenuGroup( \'testtags\' );
                    
                                // window.editor.addMenuItem(\'tag'.$model->id.'\',{
                                //        label : \''.$model->label.'\',
                                //        group : \'tags\',
                                //        command : \'insertTag'.$model->id.'\',
                                //  });
                                  
                                // window.editor.addCommand(\'insertTag'.$model->id.'\', {
                                //  exec: function(editor){  
                                //      editor.insertText(\'{'.$model->name.'}\');
                                //  }
                                // });
                                
                                
                                
                        // window.editor.contextMenu.addListener( function( element ) {
                        //            return { tag'.$model->id.': CKEDITOR.TRISTATE_OFF };
                        //    });
                        
                        window.editor.removeMenuItem("tags");
                        
                        
                        window.editor.addMenuGroup(\'tags\', 4);
                        
                        
                        '.$commandScript.'
                        
                         window.editor.addMenuItems({
                              // 2.1 add the group again, and give it getItems, return all the child items
                              tags :
                              {
                                label : \'Тэги\',
                                group : \'tags\',
                                order : 23,
                                getItems : function() {
                                  return {
                                    '.$itemsScript.'
                                  };
                                }
                              },
                        
                              // 2.2 Now add the child items to the group.
                              tag1 :
                              {
                                label : \'Мой тэг 1\',
                                group : \'tags\',
                                command : \'insertTag\',
                                order : 24
                              },
                          });
                          
                          '.$menuScript.'
                    ' ];
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить поле",
                    'content'=>'<span class="text-success">Создание поля успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];         
            }else{           
                return [
                    'title'=> "Добавить поле",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'hideTemplate' => $hideTemplate,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'hideTemplate' => $hideTemplate,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing TemplateFields model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $pjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить поле #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$pjaxContainer,
                    'title'=> "поле #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить поле #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing TemplateFields model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $pjaxContainer = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>$pjaxContainer];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing TemplateFields model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the TemplateFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TemplateFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TemplateFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
