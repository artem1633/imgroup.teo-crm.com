<?php

namespace app\filters;

use Yii;
use yii\base\ActionFilter;
use app\models\Companies;
use yii\web\ForbiddenHttpException;

/**
 * Class CompaniesFilter
 * @package app\filters
 *
 * Делает проверку, истекла ли лицензия у компании, к которой принадлежит пользователь
 */
class CompaniesFilter extends ActionFilter
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest === false)
        {
            $companyId = Yii::$app->user->identity->getCompanyId();
        } else {
            $companyId = null;
        }

        if($companyId != null)
        {
            $company = Companies::findOne($companyId);

            if($company != null)
            {
                if($company->access_end_datetime === null || (new \DateTime($company->access_end_datetime))->getTimestamp() > time()){

                } else {
                    throw new ForbiddenHttpException('Срок действия лицензии истек');
                }
            }
        }

        return parent::beforeAction($action);
    }
}