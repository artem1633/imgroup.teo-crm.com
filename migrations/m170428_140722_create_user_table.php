<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
