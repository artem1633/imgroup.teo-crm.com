<?php

use yii\db\Migration;

/**
 * Handles the creation of table `templates`.
 */
class m180617_200447_create_templates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'content' => $this->text()->comment('Контент'),
            'created_at' => $this->dateTime(),
        ]);

        $this->addCommentOnTable('templates', 'Шаблоны документов');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('templates');
    }
}
