<?php

use yii\db\Migration;

/**
 * Handles the creation of table `template_fields`.
 */
class m180617_200630_create_template_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('template_fields', [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer()->comment('Шаблон'),
            'name' => $this->string()->comment('Наименование (на англ.)'),
            'label' => $this->string()->comment('Лейбел поля'),
            'type' => $this->string()->comment('Тип поля'),
        ]);
        $this->addCommentOnTable('template_fields', 'Поля шаблона');

        $this->createIndex(
            'idx-template_fields-template_id',
            'template_fields',
            'template_id'
        );

        $this->addForeignKey(
            'fk-template_fields-template_id',
            'template_fields',
            'template_id',
            'templates',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-template_fields-template_id',
            'template_fields'
        );

        $this->dropIndex(
            'idx-template_fields-template_id',
            'template_fields'
        );

        $this->dropTable('template_fields');
    }
}
