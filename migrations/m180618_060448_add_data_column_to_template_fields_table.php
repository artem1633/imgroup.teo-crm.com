<?php

use yii\db\Migration;

/**
 * Handles adding data to table `template_fields`.
 */
class m180618_060448_add_data_column_to_template_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('template_fields', 'data', $this->string()->comment('Вспомогательные данные'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('template_fields', 'data');
    }
}
