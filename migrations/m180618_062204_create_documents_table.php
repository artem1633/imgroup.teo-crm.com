<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents`.
 */
class m180618_062204_create_documents_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('documents', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'template_id' => $this->integer()->comment('Шаблон'),
            'content' => $this->text()->comment('Контент'),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer()->comment('Пользователь'),
        ]);

        $this->createIndex(
            'idx-documents-template_id',
            'documents',
            'template_id'
        );

        $this->addForeignKey(
            'fk-documents-template_id',
            'documents',
            'template_id',
            'templates',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-documents-created_by',
            'documents',
            'created_by'
        );

        $this->addForeignKey(
            'fk-documents-created_by',
            'documents',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-documents-template_id',
            'documents'
        );

        $this->dropIndex(
            'idx-documents-template_id',
            'documents'
        );

        $this->dropForeignKey(
            'fk-documents-created_by',
            'documents'
        );

        $this->dropIndex(
            'idx-documents-created_by',
            'documents'
        );

        $this->dropTable('documents');
    }
}
