<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `documents`.
 */
class m180618_131043_add_comment_column_to_documents_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('documents', 'comment', $this->text()->comment('Комментарий'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('documents', 'comment');
    }
}
