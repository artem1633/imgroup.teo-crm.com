<?php

use yii\db\Migration;

/**
 * Handles adding api_key to table `user`.
 */
class m180620_132802_add_api_key_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'api_key', $this->string()->comment('API Ключ'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'api_key');
    }
}
