<?php

use yii\db\Migration;

/**
 * Class m180630_034937_add_columns_to_users_table
 */
class m180630_034937_add_columns_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('user', 'role_id', $this->string(255)->comment('Должность'));
    }

    public function down()
    {        
        $this->dropColumn('user', 'role_id');
    }
}
