<?php

use yii\db\Migration;

/**
 * Handles adding repeat to table `template_fields`.
 */
class m200103_105427_add_repeat_column_to_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template_fields', 'repeat', $this->integer()->defaultValue(0)->comment('Кол-во дублей'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('template_fields', 'repeat');
    }
}
