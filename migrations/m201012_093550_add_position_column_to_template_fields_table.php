<?php

use yii\db\Migration;

/**
 * Handles adding position to table `template_fields`.
 */
class m201012_093550_add_position_column_to_template_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template_fields', 'position', $this->string()->comment('Позиция'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('template_fields', 'position');
    }
}
