<?php

use yii\db\Migration;

/**
 * Handles the creation of table `document_section`.
 */
class m201013_092549_create_document_section_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('document_section', [
            'id' => $this->primaryKey(),
            'document_id' => $this->integer()->comment('Документ'),
            'label' => $this->string()->comment('Название'),
            'content' => $this->binary()->comment('Котент'),
            'sort' => $this->integer()->comment('Сортировка'),
        ]);

        $this->createIndex(
            'idx-document_section-document_id',
            'document_section',
            'document_id'
        );

        $this->addForeignKey(
            'fk-document_section-document_id',
            'document_section',
            'document_id',
            'documents',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-document_section-document_id',
            'document_section'
        );

        $this->dropIndex(
            'idx-document_section-document_id',
            'document_section'
        );

        $this->dropTable('document_section');
    }
}
