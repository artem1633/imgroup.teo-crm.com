<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "changing".
 *
 * @property int $id
 * @property string $table_name
 * @property int $line_id
 * @property string $date_time
 * @property int $user_id
 * @property string $field
 * @property string $old_value
 * @property string $new_value
 *
 * @property User $user
 */
class Changing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'changing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line_id', 'user_id'], 'integer'],
            [['date_time'], 'safe'],
            [['table_name', 'field'], 'string', 'max' => 255],
            [['old_value', 'new_value'], 'string', 'max' => 1000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Таблица',
            'line_id' => 'ИД',
            'date_time' => 'Дата и время',
            'user_id' => 'Пользователь',
            'field' => 'Поля',
            'old_value' => 'Значение',
            'new_value' => 'Изменение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
