<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document_section".
 *
 * @property int $id
 * @property int $document_id Документ
 * @property string $label Название
 * @property resource $content Котент
 * @property int $sort Сортировка
 *
 * @property Documents $document
 */
class DocumentSection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_section';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_id', 'sort'], 'integer'],
            [['content'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documents::className(), 'targetAttribute' => ['document_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_id' => 'Документ',
            'label' => 'Название',
            'content' => 'Котент',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Documents::className(), ['id' => 'document_id']);
    }
}
