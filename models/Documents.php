<?php

namespace app\models;

use app\helpers\DirHelper;
use app\helpers\StringHelper;
use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use yii\behaviors\TimestampBehavior;

use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $template_id Шаблон
 * @property string $content Контент
 * @property string $comment Комментарий
 * @property string $created_at
 *
 * @property Templates $template
 */
class Documents extends \yii\db\ActiveRecord
{
    const SCENARIO_FIELDS = 'fields';
    const SCENARIO_SECTIONS = 'sections';

    protected $generateSiteFolder;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->id;
                },
            ],
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'name'], 'required'],
            [['template_id'], 'integer'],
            [['content', 'comment'], 'string'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Templates::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'template_id' => 'Шаблон',
            'content' => 'Контент',
            'comment' => 'Комментарий',
            'created_at' => 'Создано',
        ];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null, $generateSiteFolder = false)
    {
        $this->generateSiteFolder = $generateSiteFolder;
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($this->generateSiteFolder){
            $newFolder = false;

            $contentFolder = dirname($this->template->content);
            if(is_dir($contentFolder.'/sites') == false){
                mkdir($contentFolder.'/sites');
                exec('chmod -R 777 '.$contentFolder.'/sites');
            }

            if(is_dir($contentFolder.'/sites/'.$this->id) == false){
//            DirHelper::deleteDir($contentFolder.'/sites/'.$this->id);
                mkdir($contentFolder.'/sites/'.$this->id);
                $newFolder = true;
            }


//        mkdir($contentFolder.'/sites');

            $rar_file = rar_open($this->template->content);
            $files = rar_list($rar_file);

            foreach ($files as $file)
            {
                // chmod('site', 777);
                exec('chmod -R 777 site');
                $file->extract($contentFolder.'/sites/'.$this->id);
                exec('chmod -R 777 site');
                // chmod('site', 777);
            }

            rar_close($rar_file);

            $dirFiles = scandir($contentFolder.'/sites/'.$this->id);

            $targetDir = '';

            foreach ($dirFiles as $file)
            {
                $extension = explode('.', $file);
                if(isset($extension[1])){
                    if($extension[1] != 'rar' && in_array($file, ['.', '..']) == false){
                        $targetDir = $file;
                    }
                } else {
                    if(in_array($file, ['.', '..']) == false){
                        $targetDir = $file;
                    }
                }
            }

            \Yii::warning($targetDir, 'target directory');

            /* @var DocumentsField $docFlds */
            $docFlds = DocumentsField::find()->where(['document_id' => $this->id])->all();

            foreach ($docFlds as $docFld) {
                $value = StringHelper::validateAndUploadImage($docFld->value, $this->getSiteFolderPath().'/tilda/images');

                if($value != false){
                    $docFld->value = $value;
                    $docFld->save(false);
                }
            }
            $documentsFields = DocumentsField::find()->select([
                'documents_field.value as documents_fields_value',
                'template_fields.position as template_fields_position',
            ])->where(['document_id' => $this->id])->leftJoin('template_fields', 'template_fields.id=documents_field.field_id')->asArray()->all();

            if($targetDir) {
                $configFile = null;
                $files = scandir($contentFolder.'/sites/'.$this->id."/{$targetDir}/tilda");
                foreach ($files as $file) {
                    if ($file == 'config.php') {
                        $configFile = $file;
                    }
                }

                if ($configFile != null) {
                    $configData = require("{$contentFolder}/sites/{$this->id}/{$targetDir}/tilda/{$configFile}");
                    \Yii::warning($configData, 'Config file found');

                    foreach ($documentsFields as $documentsField) {
                        ArrayHelper::setValue($configData, $documentsField['template_fields_position'], $documentsField['documents_fields_value']);
                    }

                    \Yii::warning($documentsFields, 'Config fields');


                    $str = StringHelper::varexport($configData, true);
                    $str = '<?php return $text = ' . $str . '; ?>';

                    \Yii::warning($str, 'New config');

                    $file = fopen("{$contentFolder}/sites/{$this->id}/{$targetDir}/tilda/{$configFile}", 'w');

                    fwrite($file, $str);

                    fclose($file);


                    if (file_exists("{$contentFolder}/sites/{$this->id}/{$targetDir}/index.php")) {
                        $indexHtml = file_get_contents("{$contentFolder}/sites/{$this->id}/{$targetDir}/index.php");
//                        $indexHtml = str_replace('/tilda/images/', '', $indexHtml);
//                        $indexHtml = str_replace('tilda/images/', '', $indexHtml);
                        $file = fopen("{$contentFolder}/sites/{$this->id}/{$targetDir}/index.php", 'w');

                        fwrite($file, $indexHtml);

                        fclose($file);


                        // Парсим HTML блоки из index.php
//                        DocumentSection::deleteAll(['document_id' => $this->id]);
                        if($newFolder){
                            $html = file_get_contents("{$contentFolder}/sites/{$this->id}/{$targetDir}/index.php");

                            $dom = HtmlDomParser::str_get_html($html);

                            $elements = $dom->find('#allrecords .t-rec');

                            $counter = 0;
                            foreach ($elements as $element){
                                (new DocumentSection([
                                    'document_id' => $this->id,
                                    'label' => $element->getAttribute('id'),
                                    'content' => $element->outertext(),
                                    'sort' => $counter,
                                ]))->save(false);
                                $counter++;
                            }
                        }
                    }

                }
            }
        }


        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return string
     */
    public function getPreviewHref()
    {
        $contentFolder = dirname($this->template->content);

        $targetDir = '';
        $dirFiles = scandir($contentFolder.'/sites/'.$this->id);

        foreach ($dirFiles as $file)
        {
            $extension = explode('.', $file);
            if(isset($extension[1])){
                if($extension[1] != 'rar' && in_array($file, ['.', '..']) == false){
                    $targetDir = $file;
                }
            } else {
                if(in_array($file, ['.', '..']) == false){
                    $targetDir = $file;
                }
            }
        }

        return "{$contentFolder}/sites/{$this->id}/{$targetDir}/index.php";
    }

    /**
     * @return string
     */
    public function getSiteFolderPath()
    {
        $contentFolder = dirname($this->template->content);

        $targetDir = '';
        $dirFiles = scandir($contentFolder.'/sites/'.$this->id);

        foreach ($dirFiles as $file)
        {
            $extension = explode('.', $file);
            if(isset($extension[1])){
                if($extension[1] != 'rar' && in_array($file, ['.', '..']) == false){
                    $targetDir = $file;
                }
            } else {
                if(in_array($file, ['.', '..']) == false){
                    $targetDir = $file;
                }
            }
        }

        return "{$contentFolder}/sites/{$this->id}/{$targetDir}";
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        DocumentsField::deleteAll(['document_id' => $this->id]);

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Templates::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentsFields()
    {
        return $this->hasMany(DocumentsField::className(), ['document_id' => 'id']);
    }
}
