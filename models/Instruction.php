<?php

namespace app\models;

use yii\base\Model;

/**
 * Class Instruction
 * @package app\models
 */
class Instruction extends Model
{
    const FILE_NAME = 'instruction.html';

    public $content;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'safe'],
        ];
    }

    /**
     * @return Instruction
     */
    public static function getInstruction()
    {
        if(file_exists(self::FILE_NAME))
        {
            return new self(['content' => file_get_contents(self::FILE_NAME)]);
        }

        return new self();
    }

    /**
     * @return bool|int
     */
    public function save()
    {
        return file_put_contents(self::FILE_NAME, $this->content);
    }
}