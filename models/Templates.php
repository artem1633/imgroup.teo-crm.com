<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $content Контент
 * @property string $created_at
 *
 * @property TemplateFields[] $templateFields
 */
class Templates extends \yii\db\ActiveRecord
{
    protected $flds;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'content' => 'Контент',
            'created_at' => 'Создано',
            'file' => 'Шаблон'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $file = UploadedFile::getInstanceByName('template');
        if($file != null){
            if(is_dir('site-templates') == false){
                mkdir('site-templates');
                chmod('site-templates', 777);
            }

            $dirName = Yii::$app->security->generateRandomString();
            mkdir("site-templates/{$dirName}");

            $path = "site-templates/{$dirName}/".Yii::$app->security->generateRandomString().".".$file->extension;

            $this->content = $path;

            $file->saveAs($path);


            // Распаковка конфига из архива
            $rar_file = rar_open($path);
            $files = rar_list($rar_file);

//        $entry = rar_entry_get($rar_file, 'moresmell.com/tilda/config.php');

//        VarDumper::dump($files, 10, true);

            $flds = [];

            foreach ($files as $file)
            {
                $file->extract("site-templates/{$dirName}");
                if(strpos($file->getName(), 'config.php')){
//                VarDumper::dump($file, 10, true);
                    $entry = rar_entry_get($rar_file, $file->getName());
                    $entry->extract("/site-templates/", "site-templates/{$dirName}/config.php");
                    $data = require("site-templates/{$dirName}/config.php");

                    foreach ($data as $rowName => $row)
                    {
                        if(is_array($row))
                        {
                            foreach ($row as $name => $value)
                            {
                                $label = '';
                                if(is_int($name)){
                                    if(mb_strlen($value, 'UTF-8') > 10){
                                        $label = iconv_substr($value, 0, 10, 'UTF-8');
                                    } else {
                                        $label = $value;
                                    }
                                } else {
                                    $label = $name;
                                }

                                if($value != null){
                                    if(mb_strlen($value, 'UTF-8') > 30){
                                        $value = iconv_substr($value, 0, 30, 'UTF-8');
                                    }
                                } else {
                                    $value = $label;
                                }

                                $label = str_replace(' ', '_', $label);
                                $label = str_replace('’', '', $label);
                                $label = str_replace('.', '', $label);
                                $label = str_replace(',', '', $label);
                                $label = str_replace('-', '', $label);

                                $this->flds[] = [
                                    'name' => $rowName.'_'.$label,
//                                'label' => $rowName,
                                    'label' => $value,
                                    'type' => TemplateFields::TYPE_TEXT,
                                    'position' => $rowName.'.'.$name,
                                ];
                            }
                        } else {
                            $value = '';
                            if($row != null){
                                if(mb_strlen($row, 'UTF-8') > 30){
                                    $value = iconv_substr($row, 0, 30, 'UTF-8');
                                } else {
                                    $value = $row;
                                }
                            } else {
                                $value = $row;
                            }

                            $this->flds[] = [
                                'name' => $rowName,
                                'label' => $value,
                                'type' => TemplateFields::TYPE_TEXT,
                                'position' => $rowName,
                            ];
                        }
                    }
                }
            }


            if(file_exists("site-templates/{$dirName}/config.php")){
                unlink("site-templates/{$dirName}/config.php");
            }



//        VarDumper::dump($flds, 10, true);

            rar_close($rar_file);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->flds != null){
            foreach ($this->flds as $fld){
                $templateField = new TemplateFields($fld);
                $templateField->template_id = $this->id;
                $templateField->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateFields()
    {
        return $this->hasMany(TemplateFields::className(), ['template_id' => 'id']);
    }
}
