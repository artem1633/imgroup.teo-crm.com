<?php

namespace app\models;

use Yii;

use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $password_hash
 * @property string $password
 * @property integer $status
 * @property string $email
 * @property integer $is_deletable
 * @property string $phone
 * @property string $api_key
 *
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    public $password;
    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login', 'api_key', 'is_deletable', 'password', 'password_hash'],
            self::SCENARIO_EDIT => ['name', 'login', 'api_key', 'is_deletable', 'password', 'password_hash'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
            /*['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],*/
            [['login'], 'email'],
            /*['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],*/
            [['login'], 'unique'],
            [['last_activity_datetime'], 'safe'],
            [['is_deletable'], 'integer'],
            [['login', 'password_hash', 'password', 'name', 'role_id', 'vk_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'status' => 'Статус',
            'email' => 'Email',
            'api_key' => 'API Ключ',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
            'role_id' => 'Должность',
            'last_activity_datetime' => 'Дата и время последней активности',
            'vk_id' => 'Ссылка на страницу ВКонтакте',
        ];
    }
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord)
            {                
                if (isset(Yii::$app->user->identity->id)) {
                    $log = new Logs([
                        'user_id' => Yii::$app->user->identity->id,
                        'event_datetime' => date('Y-m-d H:i:s'),
                        'event' => Logs::EVENT_USER_ADDED,
                    ]);
                    $log->description = $log->generateEventDescription();
                    $log->save();
                }
                else {
                    $log = new Logs([
                        'user_id' => null,
                        'event_datetime' => date('Y-m-d H:i:s'),
                        'event' => Logs::EVENT_USER_ADDED,
                    ]);
                    $log->description = $log->generateEventDescription();
                    $log->save();
                }
            }

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            if($this->isNewRecord && $this->api_key == null)
                $this->api_key = Yii::$app->security->generateRandomString();

            return true;
        }
        return false;
    }

    /**
     * Обновляет API Ключ
     */
    public function updateApiKey()
    {
        $this->api_key = Yii::$app->security->generateRandomString();
        $this->save(false);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangings()
    {
        return $this->hasMany(Changing::className(), ['user_id' => 'id']);
    }

    public function isSuperAdmin()
    {
        return $this->role_id === 'super_admin';
    }

//    /**
//    * Возвращает id компании, в которой состоит пользователь
//    * @return int|null
//    */
//    public function getCompanyId()
//    {
//        return $this->company_id;
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getCompanies()
//    {
//        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
//    }
//    /**
//     * Возвращает компанию
//     * @return \app\models\Companies|null
//     */
//    public function getCompanyInstance()
//    {
//        return Companies::findOne($this->company_id);
//    }
}
