<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'admin_id',
            'access_end_datetime',
            'rate_id',
        ],
    ]) ?>

</div>
