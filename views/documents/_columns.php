<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'template_id',
        'value' => 'template.name',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Templates::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => 'Выберите шаблон'
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{preview}{download}{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'download' => function ($url, $model) {
                return Html::a('<i class="fa fa-download"></i>', $url, [
                        'title'=>'Скачать',
                        'data-pjax' => 0,
                    ])."&nbsp;";
            },
            'preview' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
                        'data-pjax'=>'0', 'title'=>'Предпросмотр',
                        'data-confirm'=>false, 'data-method'=>false,
                        'target' => '_blank',
                    ])."&nbsp;";
            },
            'view' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
                    'data-pjax'=>'0', 'title'=>'Просмотр',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'comment' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-comment"></i>', $url, [
                    'data-pjax'=>'0', 'title'=>'Добавить комментарию',
                    'data-confirm'=>false, 'data-method'=>false,
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'print' => function ($url, $model) {
                return Html::a('<i class="glyphicon glyphicon-print"></i>', $url, [
                        'title'=>'Печать',
                        'data-pjax' => 0,
                        'target' => '_blank',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];   