<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TemplateFields;
use app\models\DocumentsField;

/* @var $this yii\web\View */
/* @var $model app\models\Documents */
/* @var $form yii\widgets\ActiveForm */
/* @var $fields app\models\TemplateFields[] */
/* @var $fieldsModel \yii\base\DynamicModel */

$css = <<< CSS
input[type="checkbox"] {
    width: 15px;
    margin-top: -10px;
}
CSS;

$this->registerCss($css);

?>

<div class="documents-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hidden">
        <?= $form->field($model, 'name')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'template_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'comment')->hiddenInput()->label(false) ?>
    </div>

    <?php foreach ($fields as $field): ?>

        <?php 
            $id = TemplateFields::find()->where(['name' => $field->name, 'template_id' => $model->template_id])->one()->id;
            $doc_field = DocumentsField::find()->where(['document_id' => $model->id, 'field_id' => $id])->one();
            if($doc_field == null) $value = null;
            else $value = $doc_field->value;
        ?>

        <?php if(!in_array($field->type, [TemplateFields::TYPE_DROPDOWN, TemplateFields::TYPE_TEXTAREA, TemplateFields::TYPE_HTML, TemplateFields::TYPE_POST])) { ?>

            <?= $form->field($fieldsModel, $field->name)->textInput(['type' => $field->type, 'value' => $value ])->label($field->label) ?>

        <?php } elseif($field->type === TemplateFields::TYPE_DROPDOWN) { ?>
            <?php $fieldsModel[$field->name] = $value;?>
            <?= $form->field($fieldsModel, $field->name)->dropDownList(array_combine(explode(',', $field->data), explode(',', $field->data)))->label($field->label) ?>

        <?php } else if($field->type === TemplateFields::TYPE_TEXTAREA){ ?>

            <?= $form->field($fieldsModel, $field->name)->textArea(['value' => $value])->label($field->label) ?>

        <?php } else if($field->type === TemplateFields::TYPE_HTML){ ?>
            <?php $fieldsModel[$field->name] = $value;?>
            <?= $form->field($fieldsModel, $field->name)->widget(\mihaildev\ckeditor\CKEditor::class, [])->label($field->label) ?>

            <?php // $form->field($fieldsModel, $field->name."_show")->input('checkbox')->label('Отображать')?>

        <?php } ?>

    <?php endforeach; ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
