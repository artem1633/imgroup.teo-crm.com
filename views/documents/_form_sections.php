<?php

/* @var \app\models\DocumentSection[] $sections */
/* @var \app\models\Documents $model */

use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;

$items = [];

foreach ($sections as $section)
{
    $items[$section->id] = ['content' => $section->label];
}


?>

<div class="sections-form">
    <?php $form = ActiveForm::begin(); ?>

        <div class="hidden">
            <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'name')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'template_id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'comment')->hiddenInput()->label(false) ?>
        </div>

        <?= SortableInput::widget([
            'name' => 'sections_sort',
            'items' => $items,
        ]) ?>

    <?php ActiveForm::end(); ?>
</div>
