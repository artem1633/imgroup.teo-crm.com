<?php
use rmrevin\yii\module\Comments;

?>

<div class="panel panel-inverse client-index">
    <div class="panel-heading">
        <h4 class="panel-title">Документ № <?=$id?></h4>
    </div>
    <div class="panel-body">
		<?php echo Comments\widgets\CommentListWidget::widget(['entity' => (string) $id,]);?>
    </div>
</div>
