<?php

use app\models\Documents;


/* @var $this yii\web\View */
/* @var $model app\models\Documents */
/* @var $fields app\models\TemplateFields[] */
/* @var $fieldsModel \yii\base\DynamicModel */
/* @var \app\models\DocumentSection[] $sections */

?>
<div class="documents-create">
    <?php if($model->scenario === Documents::SCENARIO_DEFAULT){ ?>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    <?php } else if($model->scenario === Documents::SCENARIO_FIELDS) { ?>

        <?= $this->render('_form_fields', [
            'model' => $model,
            'fields' => $fields,
            'fieldsModel' => $fieldsModel,
        ]) ?>

    <?php } else if($model->scenario === Documents::SCENARIO_SECTIONS) { ?>

        <?= $this->render('_form_sections', [
            'model' => $model,
            'sections' => $sections
        ]) ?>

    <?php } ?>
</div>
