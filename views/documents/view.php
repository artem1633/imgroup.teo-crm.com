<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Client;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use yii\helpers\HtmlPurifier;
use rmrevin\yii\module\Comments;
use johnitvn\ajaxcrud\BulkButtonWidget;


\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Просмотр/Изменение';

?>
    
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h2 class="panel-title">
            Документ № <?=$model->id?>
        </h2>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">Информация</span>
                            <span class="hidden-xs">Информация</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">История изменений </span>
                            <span class="hidden-xs">История изменений </span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Комментарии </span>
                            <span class="hidden-xs">Комментарии </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="default-tab-1">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'personal-pjax']) ?>   
                                <div class="col-md-12 col-sm-12">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                            <h3>Информация <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a> <a class="btn btn-primary" data-pjax="0" target="blank" href="<?=Url::toRoute(['print', 'id' => $model->id])?>"><i class="fa fa-print"></i></a></h3>
                                            <tbody>
                                                <tr>
                                                    <th><b><?=$model->getAttributeLabel('name')?></b></th>
                                                    <th><b><?=$model->getAttributeLabel('template_id')?></b></th>
                                                    <th><b><?=$model->getAttributeLabel('comment')?></b></th>
                                                    <th><b><?=$model->getAttributeLabel('created_at')?></b></th>
                                                    <th><b><?=$model->getAttributeLabel('content')?></b></th>
                                                </tr>                                                    
                                                <tr>
                                                    <td><?=Html::encode($model->name)?></td>
                                                    <td><?=Html::encode($model->template->name)?></td>
                                                    <td><?=Html::encode($model->comment)?></td>      
                                                    <td><?=Html::encode($model->created_at)?></td>       
                                                    <td><?=$model->content?></td>       
                                                </tr>
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            <?php Pjax::end() ?> 
                        </div>
                    </div>

                    <div class="tab-pane fade" id="default-tab-2">
                         <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'document-pjax']) ?>
                                    <div class="col-md-12">
                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'crud-datatable',
                                                    'dataProvider' => $dataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/changing_columns.php'),
                                                    'panelBeforeTemplate' => '',
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>BulkButtonWidget::widget([
                                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                                    ["bulk-delete-changing"] ,
                                                    [
                                                    "class"=>"btn btn-danger btn-xs",
                                                    'role'=>'modal-remote-bulk',
                                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                                    'data-request-method'=>'post',
                                                    'data-confirm-title'=>'Вы уверены?',
                                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                                    ]),
                                                    ]).
                                                    '<div class="clearfix"></div>',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-3">
                        <?php echo Comments\widgets\CommentListWidget::widget(['entity' => (string) $model->id,]);?>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>