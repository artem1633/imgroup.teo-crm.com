<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Шаблоны', 'icon' => 'fa  fa-file-o', 'url' => ['/templates'],],
//                    ['label' => 'Поля', 'icon' => 'fa  fa-pencil', 'url' => ['/template-fields'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Мои сайты', 'icon' => 'fa  fa-file-text', 'url' => ['/documents'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Инструкция', 'icon' => 'fa  fa-pencil', 'url' => ['/instruction/edit'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    [
                        'label' => 'Администратор',
                        'icon' => '',
                        'url' => '#',
                        'options' => ['class' => 'has-sub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(), 
                        'items' => [
                            ['label' => 'Компании', 'url' => ['companies/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin() ],
                        ]
                    ],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
