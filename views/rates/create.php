<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rates */

?>
<div class="rates-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
