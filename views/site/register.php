<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-earphone form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-asterisk form-control-feedback'></span>"
];
$fieldOptions5 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-star form-control-feedback'></span>"
];
$fieldOptions6 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon glyphicon-ruble form-control-feedback'></span>"
];
?>

<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span>  ТЕО-SALES
            <small>Введите данные для регистрации</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'fio', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('fio'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <?= $form
            ->field($model, 'rate_id', $fieldOptions6)
            ->label(false)
            ->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Rates::find()->all(), 'id', 'name'), ['placeholder' => $model->getAttributeLabel('rate_id'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <?= $form
            ->field($model, 'phone', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <?= $form
            ->field($model, 'login', $fieldOptions3)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('login'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions4)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <div class="login-buttons">
                <?= Html::submitButton('Регистрация', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>