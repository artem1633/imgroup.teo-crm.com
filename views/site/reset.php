<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 */

$this->title = 'Регистрация';

$fieldOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
?>

<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span>  ТЕО-SALES
            <small>Введите данные для востановления пароля</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'login', $fieldOptions)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('login'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <div class="login-buttons">
            <?= Html::submitButton('Востановить', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>