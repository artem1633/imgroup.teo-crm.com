<?php
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Статистика';
?>



    <div class="row">

        <div class="col-md-3 col-sm-12">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-key"></i></div>
                <div class="stats-info">
                    <div class="stats-title" style="color: #fff;">АВТОРИЗОВАНО</div>
                    <div class="stats-number"><?=$dataProviderAuthorized->getCount()?></div>
                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/clients/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12">
            <div class="widget widget-stats bg-red">
                <div class="stats-icon"><i class="fa fa-user"></i></div>
                <div class="stats-info">
                    <div class="stats-title" style="color: #fff;">ЗАРЕГИСТРИРОВАНО</div>
                    <div class="stats-number"><?=$dataProviderRegistered->getCount()?></div>
                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/clients/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12">
            <div class="widget widget-stats bg-orange">
                <div class="stats-icon"><i class="fa fa-user-o"></i></div>
                <div class="stats-info">
                    <div class="stats-title" style="color: #fff;">ПЕРЕШЛИ НА РЕГИСТРАЦИЮ</div>
                    <div class="stats-number"><?=$dataProviderOpenedRegistrationPage->getCount()?></div>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12">
            <div class="panel panel-inverse">
                <div class="panel-body">

                    <div class="store-remains-search">

                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                            <div class="row">
                                <div class="col-md-12 vcenter">
                                    <?= $form->field($searchModel, 'period')->dropDownList($searchModel->getPeriodsList(),
                                        ['prompt' => 'Выберите вариант']) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 vcenter">
                                    <?= Html::submitButton('Применить', ['class' => 'btn btn-primary btn-block']) ?>
                                </div>
                            </div>

                        <?php ActiveForm::end(); ?>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="row">

    </div>

<?php

$this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);

?>