<?php
use app\models\TemplateFields;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateFields */
/* @var $form yii\widgets\ActiveForm */

if($model->data != null && !is_array($model->data))
    $model->data = explode(',', $model->data);

?>

<div class="template-fields-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="<?= $hideTemplate ? 'display: none;' : '' ?>">
        <?= $form->field($model, 'template_id')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::map(\app\models\Templates::find()->all(), 'id', 'name'),
        ]) ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(TemplateFields::getTypes()) ?>

    <div id="field-data-wrapper" style="display: none;">
        <?= $form->field($model, 'data')->widget(\kartik\select2\Select2::class, [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'tags' => true,
            ],
        ]) ?>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
    $('#templatefields-type').change(function(){
        if($(this).val() === 'dropdown')
        {
             $('#field-data-wrapper').show();
        } else
        {
             $('#field-data-wrapper').hide();
        }
    });

    $('#templatefields-type').trigger('change');
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
