<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateFields */
?>
<div class="template-fields-update">

    <?= $this->render('_form', [
        'model' => $model,
        'hideTemplate' => false,
    ]) ?>

</div>
