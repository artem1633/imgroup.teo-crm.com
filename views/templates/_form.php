<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\TemplateFields;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */
/* @var $form yii\widgets\ActiveForm */

\app\assets\plugins\TemplatesAsset::register($this);

\johnitvn\ajaxcrud\CrudAsset::register($this);


$tags = [];

if($model->isNewRecord == false){
    $tags = TemplateFields::find()->where(['template_id' => $model->id])->all();
}

?>


<div class="templates-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-templates-file">
        <label for="">Шаблон</label>
        <?= Html::fileInput('template') ?>
    </div>

    <?php if($model->isNewRecord == false): ?>
        <?= $form->field($model, 'content')->textarea() ?>
    <?php endif; ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

if($model->isNewRecord == false){
    $modelId = $model->id;
} else {
    $modelId = 'null';
}


$commandScript = '';
$menuScript = '';
$itemsScript = '';


$counter = 1;
foreach ($tags as $tag)
{
    $order = 25 + $counter;

    $commandScript .= "
        editor.addCommand('insertTag{$tag->id}', {
          exec: function(editor){  
              editor.insertText('{{$tag->name}}');
          }
        });
    ";

    $menuScript .= "
        editor.addMenuItems({
              tag{$tag->id} :
              {
                label : '{$tag->label}',
                group : 'tags',
                command : 'insertTag{$tag->id}',
                order : {$order}
              },
          });
    ";

    $itemsScript .= "
        tag{$tag->id}: CKEDITOR.TRISTATE_OFF,
    ";

    $counter++;
}


$script = <<< JS
    window.editor = CKEDITOR.replace('templates-content', {
      // // Define the toolbar groups as it is a more accessible solution.
      // toolbarGroups: [{
      //     "name": "basicstyles",
      //     "groups": ["basicstyles"]
      //   },
      //   {
      //     "name": "links",
      //     "groups": ["links"]
      //   },
      //   {
      //     "name": "paragraph",
      //     "groups": ["list", "blocks"]
      //   },
      //   {
      //     "name": "document",
      //     "groups": ["mode"]
      //   },
      //   {
      //     "name": "insert",
      //     "groups": ["insert"]
      //   },
      //   {
      //     "name": "styles",
      //     "groups": ["styles"]
      //   },
      //   {
      //     "name": "about",
      //     "groups": ["about"]
      //   }
      // ],
      // // Remove the redundant buttons from toolbar groups defined above.
      // removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    });

setTimeout(function(){
    editor.addCommand('createTag', {
  exec: function(editor){
    var mySelection = editor.getSelection();

    if (CKEDITOR.env.ie) {
        mySelection.unlock(true);
        selectedText = mySelection.getNative().createRange().text;
    } else {
        selectedText = mySelection.getNative();
    }

    
    var modal = new ModalRemote('#ajaxCrudModal');
    
    modal.doRemote('/template-fields/create?word='+selectedText+'&templateId='+{$modelId}, 'GET', null);
  }
});
    
        // editor.addCommand('insertTag', {
  // exec: function(editor){
  //    
  //     editor.insertText('123123');
  // }
// });
        
        {$commandScript}

if (editor.addMenuItems) {
  // 1st, add a Menu Group
  // tip: name it the same as your plugin. (I'm not sure about this)
  editor.addMenuGroup('tick', 3);
  editor.addMenuGroup('tags', 4);

  // 2nd, use addMenuItems to add items
  editor.addMenuItems({
      // 2.1 add the group again, and give it getItems, return all the child items
      tick :
      {
        label : 'Добавить тэг',
        group : 'tick',
        order : 21,
        getItems : function() {
          return {
            tick_insertTick : CKEDITOR.TRISTATE_OFF,
            tick_insertQuestionMark : CKEDITOR.TRISTATE_OFF,
            tick_insertTickandQuestion : CKEDITOR.TRISTATE_OFF
          };
        }
      },

      // 2.2 Now add the child items to the group.
      tick_insertTick :
      {
        label : 'Добавить тэг',
        group : 'tick',
        command : 'createTag',
        order : 22
      },
  });
  
  editor.addMenuItems({
      // 2.1 add the group again, and give it getItems, return all the child items
      tags :
      {
        label : 'Тэги',
        group : 'tags',
        order : 23,
        getItems : function() {
          return {
            {$itemsScript}
          };
        }
      },

      // 2.2 Now add the child items to the group.
      tag1 :
      {
        label : 'Мой тэг 1',
        group : 'tags',
        command : 'insertTag',
        order : 24
      },
  });
  
  {$menuScript}
}

// 3rd, add Listener, and return the Menu Group
if (editor.contextMenu) {
  editor.contextMenu.addListener(function(element, selection) {
    return {
      tick : CKEDITOR.TRISTATE_OFF,
      tags : CKEDITOR.TRISTATE_OFF,
    };
  });
}

}, 1000);

JS;

$this->registerJs($script, View::POS_READY);

?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
