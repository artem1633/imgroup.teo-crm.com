<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Шаблоны";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$dataProvider->pagination = false;

?>

<div class="col-md-12">
    <p>
        <?= Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
            ['title'=> 'Добавить шаблон', 'role' => 'modal-remote', 'class'=>'btn btn-success']) ?>
    </p>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'crud-datatable-pjax']) ?>
<?php foreach ($dataProvider->models as $model): ?>

    <?php /** @var $model \app\models\Templates */ ?>

    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"><?= $model->name ?></h4>
                <div class="panel-heading-btn" style="margin-top: -20px;">
                    <?= Html::a('<i class="fa fa-eye"></i>', ['templates/preview', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-info',
                        'data-pjax' => 0, 'title'=>'Предпросмотр',
                    ]); ?>
                    <?= Html::a('<i class="fa fa-plus"></i>', ['documents/create', 'templateId' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-success',
                        'role'=>'modal-remote', 'title'=>'Создать документ',
                    ]); ?>
                    <?php
//                        echo Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
//                        'class' => 'btn btn-sm btn-icon btn-primary',
//                        'data-pjax'=>'0', 'title'=>'Изменить']);
                        ?>
                    <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-icon btn-danger',
                        'role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?']); ?>
                    <?php
//                        echo Html::a('<i class="fa fa-clone"></i>', ['copy', 'id' => $model->id], [
//                            'class' => 'btn btn-sm btn-icon btn-success',
//                            'role'=>'modal-remote', 'title'=>'Копировать',
//                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                            'data-request-method'=>'post',
//                            'data-confirm-title'=>'Вы уверены?',
//                            'data-confirm-message'=>'Вы действительно хотите скопировать запись?'
//                        ]);
                        ?>
                </div>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
