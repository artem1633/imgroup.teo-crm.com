<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */

$fields = $model->templateFields;

?>
<div class="templates-update">

    <?php Pjax::begin(['id' => 'tags-pjax-container', 'enablePushState' => false]) ?>

    <?php if($model->isNewRecord === false && count($fields) > 0): ?>

        <div class="panel panel-default overflow-hidden">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Доступные поля
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </h3>
            </div>
<!--            <div id="collapseOne" class="panel-collapse collapse in">-->
                <div class="panel-body" style="display: none">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>Тег</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($fields as $field): ?>
                            <tr>
                                <td><?=$field->label?></td>
                                <td>{<?=$field->name?>}</td>
                                <td><?= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['template-fields/delete', 'id' => $field->id, 'pjaxContainer' => '#tags-pjax-container'], [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                    ]); ?>
                                    <?= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['template-fields/update', 'id' => $field->id, 'pjaxContainer' => '#tags-pjax-container'], [
                                        'role'=>'modal-remote', 'title'=>'Изменить',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                    ]) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
<!--            </div>-->
        </div>

    <?php endif; ?>
    <?php Pjax::end() ?>



    <div class="panel panel-inverse">
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>

