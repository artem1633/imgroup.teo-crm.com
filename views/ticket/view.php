<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */
?>
<div class="ticket-view">
 
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-inverse">
                <div class="panel-body" style="height: 60vh; overflow-y: scroll;">
                    <?php \yii\widgets\Pjax::begin(['id' => 'chat-pjax-container', 'enablePushState' => false]) ?>
                    <?php foreach ($model->ticketMessages as $message): ?>
                        <?php if($message->from == \app\models\TicketMessage::FROM_USER): ?>
                            <div class="alert alert-success">
                                <h5><?=$model->user->login?></h5>
                                <p><?=$message->text?></p>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-warning">
                                <h5>Модератор</h5>
                                <p><p><?=$message->text?></p></p>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
                <div class="panel-footer">
                    <?php \yii\widgets\Pjax::begin(['id' => 'form-pjax-container', 'enablePushState' => false]) ?>
                        <?= $this->render('chat-form',[
                            'model' => $model,
                        ]) ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-body">
                    <b>Тикет</b>
                    <?= DetailView::widget(['model' => $model, 'attributes' => [
                        'subject',
                        'description',
                        [
                            'attribute' => 'created_at',
                            'format' => ['date', 'php:d M Y H:i:s'],
                        ],
                    ]]) ?>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <b>Пользователь</b>
                        <?= DetailView::widget(['model' => $model->user, 'attributes' => [
                            'login',
//                            [
//                                'attribute' => 'created_at',
//                                'format' => ['date', 'php:d M Y H:i:s'],
//                            ],
                        ]]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(
    '$("document").ready(function(){
         $("#form-pjax-container").on("pjax:end", function() {
            $.pjax.reload({container:"#chat-pjax-container"});  //Reload GridView
            return
        });
    });'
);

?>
