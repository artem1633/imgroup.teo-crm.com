<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'api_key')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= Html::tag('div','Сгенерировать ключ', ['id' => 'btn-generate', 'class' => 'btn btn-success btn-sm pull-left', 'style' => 'margin-top: 25px; margin-left: 31px;']) ?>
        </div>
    </div>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip'])->hint('
        Пароль должен содержать:
        <ul>
            <li>только латинские буквы</li>
            <li>минимум 6 символов</li>
            <li>миним одну заглавную букву</li>
            <li>миним одну цифру</li>
        </ul>
    ') ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
var makeApiKey = function(){
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 32; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  $('#user-api_key').val(text);
}

$('#btn-generate').click(makeApiKey);

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
